#!/bin/bash

# Change default properties
sed -i "/max-players=/ s/=.*/=${MAX_PLAYERS}/" server.properties && \
sed -i "/white-list=/ s/=.*/=${WHITE_LIST}/" server.properties && \
sed -i "/motd=/ s/=.*/=${SERVER_NAME}/" server.properties

# Add default OP player
echo $OP_PLAYER > ops.txt

# Start server
./ServerStart.sh