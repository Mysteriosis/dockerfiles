# ftb-endeavour - v3.2.0
Feed The Beast Endeavour modpack
made by Feed The Beast at https://feed-the-beast.com

Modpack for Minecraft 1.12.2.
Endeavour is a general all-purpose pack that is designed for solo play as well as small and medium population servers.
This pack contains a mix of magic, tech and exploration mods, and is the largest pack ever built and released by the Feed The Beast Team.

<img src="https://media-elerium.cursecdn.com/avatars/134/670/636493650464216291.png" width="338" height="338">

NOTE: In compliance with Mojang "End User License Agreement", you will need to agree to the EULA in order to run your own Minecraft server. By using this container you acknowledge the EULA! If you do not agree, then you are not permitted to use this container!
https://account.mojang.com/documents/minecraft_eula

This Dockerfile will always download latest build of Feed The Beast Endeavour 2.1.0 when building.

The worldname must be the default "world". 
Settings will reset when upgrading.
Access the console to op and whitelist.

Running ftb-endeavour data container:
```
docker run --name [name of your data container] mysteriosis/ftb-endeavour:2.1.0 echo 'Data-only container'
```

Running ftb-endeavour server:
```
docker run --tty=true --interactive=true --detach=true --name=[name of your container] --volumes-from [name of your data container] --publish=[port on your host]:25565 mysteriosis/ftb-endeavour:2.1.0
```

When upgrading sometime items have been remove and therefor you have to confirm removal. </br>
To do so run `docker attach [name of your container]` and type `/fml confirm` when prompted to confirm or cancel. </br>
Exit with CTRL+P CTRL+Q. </br>

To access the console:  
- `docker attach [name of container]`
- Run your commands
- To exit: CTRL+P CTRL+Q

The first time the server starts it creates the server.properties file with default settings and spawns "world". 
Not recommended to change these settings by hand.

When upgrading:
To upgrade delete ftb-endeavour server and start new ftb-endeavour server with `--volumes-from [name of your data container]`.
Settings will reset when upgrading.
Access the console to op and whitelist.