FROM java:8-jre

# INFORMATIONS
LABEL AUTHOR="Jonas Bonno Mikkelsen (https://github.com/JonasBonno)"
LABEL MAINTAINER="Pierre-Alain Curty (https://gitlab.com/Mysteriosis)"
LABEL VERSION=1.4
LABEL REVELATION_VERSION=2.6.0

# BUILD ARGUMENTS
ARG MINECRAFT_VERSION='1.12.2'
ARG FORGE_VERSION='2768'
ARG SPONGE_VERSION='1.12.2-2768-7.1.5-RC3514'
ARG SPONGE=1
ARG NUCLEUS=0

# JAVA PARAMS
ENV MAX_RAM '6G'
ENV JAVA_PARAMETERS '-XX:+UseG1GC -Xms6G -Dsun.rmi.dgc.server.gcInterval=2147483646 -XX:+UnlockExperimentalVMOptions -XX:G1NewSizePercent=20 -XX:G1ReservePercent=20 -XX:MaxGCPauseMillis=50 -XX:G1HeapRegionSize=32M -Dfml.queryResult=confirm'

# GAME OPTION
ENV OP_PLAYER ''
ENV SERVER_NAME 'A FTBRevelation Server'
ENV WHITE_LIST false
ENV MAX_PLAYERS 10

# USE BASH
SHELL ["/bin/bash", "-c"]

RUN echo "SPONGE: $SPONGE"
RUN echo "NUCLEUS: $NUCLEUS"

# Updating container
RUN apt-get update \
    && apt-get upgrade --yes --force-yes \
    && apt-get clean \ 
    && rm -rf /var/lib/apt/lists/* 

# Setting workdir
WORKDIR /minecraft

# Changing user to root
USER root

COPY server.properties /minecraft/
COPY start.sh /minecraft/

# Creating user and downloading files
RUN useradd -m -U minecraft \
    && mkdir -p /minecraft/world \
    && curl -L https://www.feed-the-beast.com/projects/ftb-revelation/files/2637817/download -o FTBRevelationServer_2.6.0.zip \
    && unzip FTBRevelationServer_2.6.0.zip \
    && rm FTBRevelationServer_2.6.0.zip \
    && chmod u+x FTBInstall.sh ServerStart.sh start.sh \
    && echo "#By changing the setting below to TRUE you are indicating your agreement to our EULA (https://account.mojang.com/documents/minecraft_eula)." > eula.txt \
    && echo "$(date)" >> eula.txt \
    && echo "eula=TRUE" >> eula.txt

# Add Sponge
RUN if [ "$SPONGE" == "1" ]; then \
    curl https://repo.spongepowered.org/maven/org/spongepowered/spongeforge/$SPONGE_VERSION/spongeforge-$SPONGE_VERSION.jar -o spongeforge-$SPONGE_VERSION.jar \
    && mv spongeforge-$SPONGE_VERSION.jar mods/ \
    && echo " ==> Sponge added."; \
    else echo " ==> Sponge not added."; \
    fi

# Add Nucleus (Sponge plugin)
RUN if [ "$SPONGE" == "1" ] && [ "$NUCLEUS" == "1" ]; then \
    curl https://github.com/NucleusPowered/Nucleus/releases/download/1.8/Nucleus-1.8.0-S7.1-MC1.12.2-plugin.jar -o Nucleus-1.5.5-S7.0-MC1.12.2-plugin.jar \
    && mv Nucleus-1.5.5-S7.0-MC1.12.2-plugin.jar mods/ \
    && echo " ==> Nucleus added."; \
    else echo " ==> Nucleus not added."; \
    fi

# Server configuration
RUN echo "export MAX_RAM=${MAX_RAM}" > settings-local.sh \
    && echo "export JAVA_PARAMETERS='${JAVA_PARAMETERS}'" >> settings-local.sh

COPY start.sh /minecraft/

# Give ownership to minecraft user
RUN chown -R minecraft:minecraft /minecraft

USER minecraft

# Running install
RUN /minecraft/FTBInstall.sh

# Expose port 25565
EXPOSE 25565

# Expose volume
VOLUME ["/minecraft/world"]

# Start server
ENTRYPOINT ["/bin/bash",  "/minecraft/start.sh"]